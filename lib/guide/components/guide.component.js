"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _onboarding = require("@atlaskit/onboarding");

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var Guide =
/*#__PURE__*/
function (_React$Component) {
  (0, _inherits2.default)(Guide, _React$Component);

  function Guide() {
    (0, _classCallCheck2.default)(this, Guide);
    return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Guide).apply(this, arguments));
  }

  (0, _createClass2.default)(Guide, [{
    key: "render",
    value: function render() {
      return _react.default.createElement(_onboarding.SpotlightManager, null, this.props.children);
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);
  return Guide;
}(_react.default.Component);

Guide.defaultProps = {
  children: _react.default.createElement(_react.default.Fragment, null)
};
Guide.propTypes = {
  children: _propTypes.default.element
};
var _default = Guide;
var _default2 = _default;
exports.default = _default2;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Guide, "Guide", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guide.component.jsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\components\\guide.component.jsx");
  leaveModule(module);
})();

;