"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isGuideWasNotSeen = isGuideWasNotSeen;
exports.getAllSpotlightsFromUnseenGuides = getAllSpotlightsFromUnseenGuides;
exports.getInitValue = getInitValue;
exports.mapGuideSchemesToReactComponents = mapGuideSchemesToReactComponents;
exports.getDataFromSpotlightReactComponent = getDataFromSpotlightReactComponent;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _react = _interopRequireDefault(require("react"));

var _shortid = _interopRequireDefault(require("shortid"));

var _guideSpotlight = _interopRequireDefault(require("../components/guideSpotlight.component"));

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var ACTIONS = {
  close: {
    onClick: 'finish',
    text: 'Close'
  },
  next: {
    onClick: 'next',
    text: 'Next'
  },
  back: {
    onClick: 'back',
    text: 'Back'
  },
  skip: {
    onClick: 'finish',
    text: 'Skip'
  },
  remindLater: {
    onClick: 'remindLater',
    text: 'Remind later'
  }
};

function deleteLastSpotlight(spotlights) {
  return spotlights.slice(0, spotlights.length - 1);
}

function filterFinishMethods(actions) {
  return actions.filter(function (_ref) {
    var onClick = _ref.onClick;
    return !['finish', 'close', 'remindLater'].some(function (e) {
      return e === onClick;
    });
  });
}

function isNextMethodExist(actions) {
  return actions.some(function (_ref2) {
    var onClick = _ref2.onClick;
    return onClick === 'next';
  });
}

function isBackMethodExist(actions) {
  return actions.some(function (_ref3) {
    var onClick = _ref3.onClick;
    return onClick === 'back';
  });
}

function addNextMethod(actions) {
  var nextAction = ACTIONS.next;

  if (isNextMethodExist(actions)) {
    return actions;
  }

  return [].concat((0, _toConsumableArray2.default)(actions), [nextAction]);
}

function addBackMethod(actions) {
  var backAction = ACTIONS.back;

  if (isBackMethodExist(actions)) {
    return actions;
  }

  return [backAction].concat((0, _toConsumableArray2.default)(actions));
}

function convertLastSpotlightFinishToNextMethod(spotlights) {
  var firstSpotlight = spotlights[0];
  var isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return (0, _objectSpread2.default)({}, firstSpotlight, {
      props: (0, _objectSpread2.default)({}, firstSpotlight.props, {
        actions: (0, _toConsumableArray2.default)(addNextMethod(firstSpotlight.props.actions))
      })
    });
  }

  var lastSpotlight = spotlights[spotlights.length - 1];
  return (0, _objectSpread2.default)({}, lastSpotlight, {
    props: (0, _objectSpread2.default)({}, lastSpotlight.props, {
      actions: (0, _toConsumableArray2.default)(addNextMethod(filterFinishMethods(lastSpotlight.props.actions)))
    })
  });
}

function convertFirstSpotlightFinishToBackMethod(spotlights) {
  var spotlight = spotlights[0];
  var isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return (0, _objectSpread2.default)({}, spotlight, {
      props: (0, _objectSpread2.default)({}, spotlight.props, {
        actions: (0, _toConsumableArray2.default)(addBackMethod(spotlight.props.actions))
      })
    });
  }

  return (0, _objectSpread2.default)({}, spotlight, {
    props: (0, _objectSpread2.default)({}, spotlight.props, {
      actions: (0, _toConsumableArray2.default)(addBackMethod(filterFinishMethods(spotlight.props.actions)))
    })
  });
}

function deleteFirstSpotlight(spotlights) {
  return spotlights.slice(1, spotlights.length);
}

function isGuideWasNotSeen(seenGuidesVersions, guide) {
  return !seenGuidesVersions.some(function (v) {
    return v === guide.version;
  });
}

function prepareFirstGuide(spotlights) {
  return [].concat((0, _toConsumableArray2.default)(deleteLastSpotlight(spotlights)), [convertLastSpotlightFinishToNextMethod(spotlights)]);
}

function prepareLastGuide(spotlights) {
  return [convertFirstSpotlightFinishToBackMethod(spotlights)].concat((0, _toConsumableArray2.default)(deleteFirstSpotlight(spotlights)));
}

function prepareMiddleGuide(spotlights) {
  var spotlightsWithoutFirstAndLastElements = spotlights.slice(1, spotlights.length - 1);

  if (spotlights.length === 1) {
    var firstSpotlightWithBackAndNextMethod = spotlights.map(function (spot) {
      return (0, _objectSpread2.default)({}, spot, {
        props: (0, _objectSpread2.default)({}, spot.props, {
          actions: (0, _toConsumableArray2.default)(filterFinishMethods((0, _toConsumableArray2.default)(addBackMethod((0, _toConsumableArray2.default)(addNextMethod(spot.props.actions))))))
        })
      });
    });
    return [firstSpotlightWithBackAndNextMethod[0]];
  }

  return [convertFirstSpotlightFinishToBackMethod(spotlights)].concat((0, _toConsumableArray2.default)(spotlightsWithoutFirstAndLastElements), [convertLastSpotlightFinishToNextMethod(spotlights)]);
}

function getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides) {
  var mergedSpotlights = [];
  var versions = [];
  var unseendGuides = guides.filter(function (guide) {
    return isGuideWasNotSeen(seenGuidesVersions, guide);
  });

  if (unseendGuides.length === 1) {
    return {
      spotlights: unseendGuides[0].spotlights,
      versions: [unseendGuides[0].version]
    };
  }

  unseendGuides.forEach(function (guide, index) {
    var spotlights = guide.spotlights;
    var version = guide.version;
    versions.push(version);
    var isFirstGuide = index === 0;
    var isLastGuide = index === unseendGuides.length - 1;

    if (isFirstGuide) {
      spotlights = prepareFirstGuide(spotlights);
    } else if (isLastGuide) {
      spotlights = prepareLastGuide(spotlights);
    } else {
      spotlights = prepareMiddleGuide(spotlights);
    }

    mergedSpotlights.push.apply(mergedSpotlights, (0, _toConsumableArray2.default)(spotlights));
  });
  return {
    spotlights: mergedSpotlights,
    versions: versions
  };
}

function getInitValue(spotlights) {
  return spotlights.length ? 0 : null;
}

function mapActionsNameToObjects(actions) {
  return actions.map(function (action) {
    return ACTIONS[action];
  });
}

function generateSpotlightKey() {
  return "spotlight-".concat(_shortid.default.generate());
}

function mapSpotlightSchemeJsonToReactComponents(spotlightSchemes) {
  return spotlightSchemes.map(function (scheme) {
    var mappedScheme = (0, _objectSpread2.default)({}, scheme, {
      actions: mapActionsNameToObjects(scheme.actions)
    });
    return _react.default.createElement(_guideSpotlight.default, (0, _extends2.default)({
      key: generateSpotlightKey()
    }, mappedScheme));
  });
}

function mapGuideSchemesToReactComponents(guideSchemes) {
  return guideSchemes.map(function (guidScheme) {
    var version = guidScheme.version,
        spotlightsSchemes = guidScheme.spotlightsSchemes;
    return {
      version: version,
      spotlights: mapSpotlightSchemeJsonToReactComponents(spotlightsSchemes)
    };
  });
}

function getDataFromSpotlightReactComponent(spotlight) {
  var _spotlight$props = spotlight.props,
      header = _spotlight$props.header,
      position = _spotlight$props.position,
      content = _spotlight$props.content,
      target = _spotlight$props.target,
      width = _spotlight$props.width;
  return {
    header: header,
    position: position,
    content: content,
    target: target,
    width: width
  };
}

;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ACTIONS, "ACTIONS", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteLastSpotlight, "deleteLastSpotlight", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(filterFinishMethods, "filterFinishMethods", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isNextMethodExist, "isNextMethodExist", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isBackMethodExist, "isBackMethodExist", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addNextMethod, "addNextMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addBackMethod, "addBackMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertLastSpotlightFinishToNextMethod, "convertLastSpotlightFinishToNextMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertFirstSpotlightFinishToBackMethod, "convertFirstSpotlightFinishToBackMethod", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteFirstSpotlight, "deleteFirstSpotlight", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isGuideWasNotSeen, "isGuideWasNotSeen", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareFirstGuide, "prepareFirstGuide", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareLastGuide, "prepareLastGuide", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareMiddleGuide, "prepareMiddleGuide", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getAllSpotlightsFromUnseenGuides, "getAllSpotlightsFromUnseenGuides", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getInitValue, "getInitValue", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapActionsNameToObjects, "mapActionsNameToObjects", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(generateSpotlightKey, "generateSpotlightKey", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapSpotlightSchemeJsonToReactComponents, "mapSpotlightSchemeJsonToReactComponents", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapGuideSchemesToReactComponents, "mapGuideSchemesToReactComponents", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getDataFromSpotlightReactComponent, "getDataFromSpotlightReactComponent", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  leaveModule(module);
})();

;