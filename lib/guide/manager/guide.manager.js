"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _onboarding = require("@atlaskit/onboarding");

var _guide = require("../util/guide.util");

var _react = _interopRequireDefault(require("react"));

(function () {
  var enterModule = require('react-hot-loader').enterModule;

  enterModule && enterModule(module);
})();

var GuideManager =
/*#__PURE__*/
function () {
  function GuideManager(reactComponent, guides, guideRepository) {
    (0, _classCallCheck2.default)(this, GuideManager);
    this.reactComponent = reactComponent;
    this.guideRepository = guideRepository;
    this.versions = [];
    this.spotlights = [];
    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.finish = this.finish.bind(this);
    this.remindLater = this.remindLater.bind(this);
    this.functions = {
      next: this.next,
      finish: this.finish,
      back: this.back
    };
    this.setup(guides);

    this.onChange = function () {};

    this.onFinish = function () {};

    this.onStart = function () {};

    this.started = false;
    this.finished = false;
  }

  (0, _createClass2.default)(GuideManager, [{
    key: "setOnChange",
    value: function setOnChange(func) {
      this.onChange = func;
    }
  }, {
    key: "setOnFinish",
    value: function setOnFinish(func) {
      this.onFinish = func;
    }
  }, {
    key: "setOnStart",
    value: function setOnStart(func) {
      this.onStart = func;
    }
  }, {
    key: "enableGuideStarted",
    value: function enableGuideStarted() {
      if (this.spotlights.length > 0) {
        this.started = true;
      } else {
        this.finish();
      }
    }
  }, {
    key: "setup",
    value: function setup(guides) {
      var _this = this;

      this.initializeActiveSpotlightsState();
      return this.guideRepository.getAllOfVersionsOfSeenGuides().then(function (seenGuideVersions) {
        var unseenGuides = (0, _guide.getAllSpotlightsFromUnseenGuides)(seenGuideVersions, guides);
        var spotlights = unseenGuides.spotlights,
            versions = unseenGuides.versions;
        _this.spotlights = _this.addMethodsToSpotlights(spotlights);
        _this.versions = versions;

        _this.enableGuideStarted();

        _this.setSpotlightNumberOnStore((0, _guide.getInitValue)(spotlights));

        return seenGuideVersions;
      }).catch(console.warn);
    }
  }, {
    key: "isStateNoInitialized",
    value: function isStateNoInitialized() {
      return !this.reactComponent.state;
    }
  }, {
    key: "initializeActiveSpotlightsState",
    value: function initializeActiveSpotlightsState() {
      var value = null;
      var reactComponent = this.reactComponent;

      if (this.isStateNoInitialized()) {
        reactComponent.state = {
          activeSpotlight: value
        };
        return;
      }

      reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: value
        });
      });
    }
  }, {
    key: "setSpotlightNumberOnStore",
    value: function setSpotlightNumberOnStore(initValue) {
      this.reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: initValue
        });
      });
    }
  }, {
    key: "next",
    value: function next() {
      var activeSpotlight = this.reactComponent.state.activeSpotlight;
      var nextSpotlight = activeSpotlight + 1;
      this.reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: nextSpotlight
        });
      });
      this.onChange(nextSpotlight, (0, _guide.getDataFromSpotlightReactComponent)(this.spotlights[nextSpotlight]), true);
    }
  }, {
    key: "back",
    value: function back() {
      var activeSpotlight = this.reactComponent.state.activeSpotlight;
      var previousSpotlight = activeSpotlight - 1;
      this.reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: previousSpotlight
        });
      });
      this.onChange(previousSpotlight, (0, _guide.getDataFromSpotlightReactComponent)(this.spotlights[previousSpotlight]), false);
    }
  }, {
    key: "finish",
    value: function finish() {
      this.reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: null
        });
      });
      this.onFinish();
      this.finished = true;
      this.guideRepository.saveVersionsOfSeenGuides(this.versions);
    }
  }, {
    key: "remindLater",
    value: function remindLater() {
      this.reactComponent.setState(function (state) {
        return (0, _objectSpread2.default)({}, state, {
          activeSpotlight: null
        });
      });
      this.onFinish();
    }
  }, {
    key: "renderSpotlight",
    value: function renderSpotlight() {
      var activeSpotlight = this.reactComponent.state.activeSpotlight;

      if (this.started) {
        this.started = false;
        this.onStart((0, _guide.getDataFromSpotlightReactComponent)(this.spotlights[activeSpotlight]));
      }

      return this.finished ? _react.default.createElement(_react.default.Fragment, null) : _react.default.createElement(_onboarding.SpotlightTransition, null, this.spotlights[activeSpotlight]);
    }
  }, {
    key: "mapStrToFunctions",
    value: function mapStrToFunctions(actions) {
      var _this2 = this;

      return actions.map(function (action) {
        var onClick = _this2.functions[action.onClick];

        if (!onClick) {
          console.error("Unrecognized guide function by name: ".concat(action.onClick, "."));
        }

        return (0, _objectSpread2.default)({}, action, {
          onClick: onClick
        });
      });
    }
  }, {
    key: "addMethodsToSpotlights",
    value: function addMethodsToSpotlights(spotlights) {
      var _this3 = this;

      return spotlights.map(function (spotlight) {
        return (0, _objectSpread2.default)({}, spotlight, {
          props: (0, _objectSpread2.default)({}, spotlight.props, {
            actions: _this3.mapStrToFunctions(spotlight.props.actions)
          })
        });
      });
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);
  return GuideManager;
}();

exports.default = GuideManager;
;

(function () {
  var reactHotLoader = require('react-hot-loader').default;

  var leaveModule = require('react-hot-loader').leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideManager, "GuideManager", "D:\\Project\\any\\APDCGuide\\src\\main\\js\\guide\\manager\\guide.manager.jsx");
  leaveModule(module);
})();

;