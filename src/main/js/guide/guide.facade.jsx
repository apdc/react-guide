import GuideManager from './manager/guide.manager';

export default class GuideFacade {
  constructor(reactComponent, guides, guideRepository) {
    this.guideManager = new GuideManager(
      reactComponent,
      guides,
      guideRepository,
    );
  }

  onChange(func) {
    this.guideManager.setOnChange(func);
  }

  onFinish(func) {
    this.guideManager.setOnFinish(func);
  }

  onStart(func) {
    this.guideManager.setOnStart(func);
  }

  getSpotlights() {
    return this.guideManager.spotlights;
  }

  renderGuideTour() {
    return this.guideManager.renderSpotlight();
  }
}
